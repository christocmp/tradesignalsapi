#code for prorealtime prescreens - manually add this result to mt4 screeners


Timeframe example
###################
TIMEFRAME(weekly)
Condition1 = Williams[14](Close) < 0 AND Williams[14](Close) > -20
TIMEFRAME(30 minutes)
Condition2 = ExponentialAverage[20](Close) CROSSES OVER ExponentialAverage[12](Close)
SCREENER[Condition1 AND Condition2]



DOWN based on hourly and daily moving averages
################################################

TIMEFRAME(daily)
indicator1 = Average[20](close)
indicator2 = Average[50](close)
c1 = (indicator1 < indicator2)

indicator3 = Average[50](close)
indicator4 = Average[200](close)
c2 = (indicator3 < indicator4)

indicator5 = Average[20](close)
indicator6 = Average[20](close)
c3 = (indicator5 < indicator6[1])

dailyTrendDown = c1 AND c2 AND c3

TIMEFRAME(1 hour)
indicator7 = Average[20](close)
indicator8 = Average[50](close)
c4 = (indicator7 < indicator8)

indicator9 = Average[50](close)
indicator10 = Average[200](close)
c5 = (indicator9 < indicator10)

indicator11 = Average[20](close)
indicator12 = Average[20](close)
c6 = (indicator11 < indicator12[1])

hourlyTrendDown = c4 AND c5 AND c6

SCREENER[hourlyTrendDown AND dailyTrendDown] ((close/DClose(1)-1)*100 AS "%Chg yest.")


UP based on daily and hourly moving averages
##############################################

TIMEFRAME(daily)
indicator1 = Average[20](close)
indicator2 = Average[50](close)
c1 = (indicator1 > indicator2)

indicator3 = Average[50](close)
indicator4 = Average[200](close)
c2 = (indicator3 > indicator4)

indicator5 = Average[20](close)
indicator6 = Average[20](close)
c3 = (indicator5 > indicator6[1])

dailyTrendDown = c1 AND c2 AND c3

TIMEFRAME(1 hour)
indicator7 = Average[20](close)
indicator8 = Average[50](close)
c4 = (indicator7 > indicator8)

indicator9 = Average[50](close)
indicator10 = Average[200](close)
c5 = (indicator9 > indicator10)

indicator11 = Average[20](close)
indicator12 = Average[20](close)
c6 = (indicator11 > indicator12[1])

hourlyTrendDown = c4 AND c5 AND c6

SCREENER[hourlyTrendDown AND dailyTrendDown] ((close/DClose(1)-1)*100 AS "%Chg yest.")

Old up indicator
#################
indicator1 = Average[20](close)
indicator2 = Average[50](close)
c1 = (indicator1 > indicator2)

indicator3 = Average[50](close)
indicator4 = Average[200](close)
c2 = (indicator3 > indicator4)

indicator5 = Average[20](close)
indicator6 = indicator5
c3 = (indicator5 > indicator6[1])

SCREENER[c1 AND c2 AND c3] ((close/DClose(1)-1)*100 AS "%Chg yest.")
