# Trade Signals API

RESTful API to log trade signals for later processing

## config.json format

{
  "database" : {
    "username": "",
    "password": "",
    "host": "",
    "port": "",
    "name": ""
  }
}