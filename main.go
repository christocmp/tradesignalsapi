package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"
)

// The person Type
type TradeSignal struct {
	Instrument     string `json:"instrument"`
	SignalDatetime int32  `json:"timestamp"`
	SignalType     string `json:"signalType"`
	AdditionalData string `json:"additionalData"`
	Direction      string `json:"direction"`
	PriceLevel     string `json:"pricelevel"`
}

//config structs
type Config struct {
	Database DatabaseConfig
}

type DatabaseConfig struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Host     string `json:"host"`
	Port     string `json:"port"`
	Name     string `json:"name"`
}

type User struct {
	id    int
	email string
}

const portNumber = "7001"

var config Config
var db *sql.DB

func loadConfig() {
	configFile, err := os.Open("config.json")
	defer configFile.Close()

	if err != nil {
		log.Println(err)
		panic("Failed to load config.json")
	}

	log.Println("Reading config from config.json")

	byteValue, _ := ioutil.ReadAll(configFile)
	json.Unmarshal(byteValue, &config)
}

func main() {
	var err error

	loadConfig()

	//listen to OS todo signal processing needs to be tested
    //https://scalingo.com/articles/2014/12/19/graceful-server-restart-with-go.html
	// setup signal catching
	sigs := make(chan os.Signal, 1) //signal catching does not work on windows

	signal.Notify(sigs)
	//signal.Notify(sigs,syscall.SIGQUIT)
	// method invoked upon seeing signal
	go func() {
		s := <-sigs
		log.Printf("RECEIVED SIGNAL: %s", s)
		destruct()
		os.Exit(1)
	}()

	//setup db
	dbConfig := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", config.Database.Username, config.Database.Password, config.Database.Host, config.Database.Port, config.Database.Name)
	db, err = sql.Open("mysql", dbConfig)

	defer db.Close() //db manages connection pool automatically defer will defer close will close this connection on return

	if err != nil {
		db.Close()
		os.Exit(3)
	}

	router := mux.NewRouter()
	router.HandleFunc("/tradesignal", createSignal).Methods("POST")
	router.HandleFunc("/datetest", dateTest).Methods("GET")
	log.Println("Listening on: " + portNumber)
	configPort := ":" + portNumber
	log.Fatal(http.ListenAndServe(configPort, router))
}

//first run checks hmm maybe this should be a constructor
func construct() {
	err := db.Ping()
	if err != nil {
		panic(err)
		log.Println(err.Error())
	}
}

func destruct() {
	log.Println("Cleaning up all the processes")
	return
}

func createSignal(w http.ResponseWriter, r *http.Request) {
	log.Printf("Saving trade signal....\n")

	decoder := json.NewDecoder(r.Body)
	var tradeSignal TradeSignal
	var result [2]string
	err := decoder.Decode(&tradeSignal)

	if err != nil {
		w.Header().Set("Content-Type", "text/plain; charset=utf-8") // normal header
		w.WriteHeader(http.StatusBadRequest)
		io.WriteString(w, "Unable to create signal struct check request body")
		panic(err)
	}

	log.Printf("Instrument: " + tradeSignal.Instrument + "\n")
	log.Printf("Timestamp: %d \n", tradeSignal.SignalDatetime)
	log.Printf("Signaltype: " + tradeSignal.SignalType + "\n")
	log.Printf("Direction: " + tradeSignal.Direction + "\n")
	log.Printf("Additional data: " + tradeSignal.AdditionalData + "\n")
	log.Printf("Price level: " + tradeSignal.PriceLevel + "\n")

	result = saveRecord(tradeSignal)

	if result[0] != "200" {
		w.Header().Set("Content-Type", "text/plain; charset=utf-8") // normal header
		responseCode, err := strconv.ParseInt(result[0], 10, 16)
		w.WriteHeader(int(responseCode))
		io.WriteString(w, result[1])
		if err != nil {
			panic(err)
		}
	} else {
		w.Header().Set("Content-Type", "text/plain; charset=utf-8") // normal header
		responseCode, err := strconv.ParseInt(result[0], 10, 16)
		w.WriteHeader(int(responseCode))
		io.WriteString(w, result[1])
		if err != nil {
			panic(err)
		}
	}
}

func dateTest(w http.ResponseWriter, r *http.Request) {
	datetime := time.Now().Format(time.RFC3339)
	log.Printf("Date time..." + datetime + "\n")
	return
}

func saveRecord(signal TradeSignal) [2]string {
	var err error
	var errorMessage [2]string // used to build htp response (0 - http code, 1 - message)

	log.Printf("Saving trade signal....\n")

	//Check for duplicate signal
	log.Printf("Checking for duplicates %s\n", signal.Instrument)

	var found_id int64
	row := db.QueryRow("SELECT id FROM raw_signals WHERE instrument = ? AND signal_datetime = ? AND signal_type = ?", signal.Instrument, signal.SignalDatetime, signal.SignalType).Scan(&found_id)

	switch {
	case row == sql.ErrNoRows:
		log.Printf("duplicate row not found")
	case row != nil && row != sql.ErrNoRows:
		errorMessage[0] = "500"
		errorMessage[1] = "Unable to connect to db"
		log.Printf("%s - %s", errorMessage[0], errorMessage[1])
		log.Print(row)
		return errorMessage
	default:
		errorMessage[0] = "409"
		errorMessage[1] = "Signal already exists"
		log.Printf("%s - %s", errorMessage[0], errorMessage[1])
		return errorMessage
	}

	datetime := time.Now()

	stmt, err := db.Prepare("INSERT INTO raw_signals (instrument, signal_datetime, signal_type, direction, price_level) VALUES (?,?,?,?,?);")

	if err != nil {
		log.Fatal(err)
	}

	rows, err := stmt.Exec(
		signal.Instrument,
		datetime, //signal.SignalDatetime,
		signal.SignalType,
		signal.Direction,
		signal.PriceLevel)

	if err != nil {
		log.Fatal(err)
	} else {
		count, err2 := rows.RowsAffected()
		if err != nil {
			log.Fatal(err2)
		} else {
			errorMessage[0] = "200"
			errorMessage[1] = fmt.Sprintf("rows inserted: %d\n", count)
			log.Printf("%s - %s", errorMessage[0], errorMessage[1])
		}
	}
	return errorMessage
}
